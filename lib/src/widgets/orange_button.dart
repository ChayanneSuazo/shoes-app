import 'package:flutter/material.dart';

class OrangeButton extends StatelessWidget {
  final String title;
  final double high;
  final double width;
  final Color color;

  const OrangeButton(
      {required this.title,
      this.high = 50,
      this.width = 150,
      this.color = Colors.orange});

  @override
  Widget build(BuildContext context) {
    return Container(
      alignment: Alignment.center,
      width: width,
      height: high,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(100),
        color: color,
      ),
      child: Text(
        title,
        style: const TextStyle(color: Colors.white),
      ),
    );
  }
}
