import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:shoes_app/src/models/shoe_model.dart';
import 'package:shoes_app/src/pages/shoe_description_page.dart';

class ShoeSizePreview extends StatelessWidget {
  final bool fullScreen;

  const ShoeSizePreview({Key? key, this.fullScreen = false}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        if (!fullScreen) {
          Navigator.push(
            context,
            MaterialPageRoute(
                builder: (BuildContext context) => ShoeDescriptionPage()),
          );
        }
      },
      child: Padding(
        padding: EdgeInsets.symmetric(
          horizontal: (fullScreen) ? 5 : 30,
          vertical: (fullScreen) ? 3 : 0,
        ),
        child: Container(
          width: double.infinity,
          height: (fullScreen) ? 430 : 430,
          decoration: BoxDecoration(
            color: const Color(0xffFFCF53),
            borderRadius: (!fullScreen)
                ? BorderRadius.circular(50)
                : const BorderRadius.only(
                    bottomLeft: Radius.circular(50),
                    bottomRight: Radius.circular(50),
                    topLeft: Radius.circular(40),
                    topRight: Radius.circular(40)),
          ),
          child: Column(
            children: [
              _ShoeWithShade(),
              if (!fullScreen) _ShoeSize(),
            ],
          ),
        ),
      ),
    );
  }
}

class _ShoeWithShade extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final shoeModelProvider = Provider.of<ShoeModel>(context);
    return Padding(
      padding: const EdgeInsets.all(50),
      child: Stack(
        children: [
          Positioned(
            bottom: 20,
            right: 0,
            child: _ShoeShade(),
          ),
          Image(
            image: AssetImage(shoeModelProvider.assetImage),
          )
        ],
      ),
    );
  }
}

class _ShoeShade extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Transform.rotate(
      angle: -0.5,
      child: Container(
        width: 230,
        height: 120,
        decoration: BoxDecoration(
            // color: Colors.red,
            borderRadius: BorderRadius.circular(100),
            boxShadow: const [
              BoxShadow(color: Color(0xffEAA14E), blurRadius: 40)
            ]),
      ),
    );
  }
}

class _ShoeSize extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 10.0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: const [
          _ShoeSizeBox(size: 7),
          _ShoeSizeBox(size: 7.5),
          _ShoeSizeBox(size: 8),
          _ShoeSizeBox(size: 8.5),
          _ShoeSizeBox(size: 9),
          _ShoeSizeBox(size: 9.5),
        ],
      ),
    );
  }
}

class _ShoeSizeBox extends StatelessWidget {
  final double size;

  const _ShoeSizeBox({required this.size});

  @override
  Widget build(BuildContext context) {
    final shoeModelProvider = Provider.of<ShoeModel>(context);
    return GestureDetector(
      onTap: () {
        final shoeModelProvider =
            Provider.of<ShoeModel>(context, listen: false);
        shoeModelProvider.size = size;
      },
      child: Container(
        alignment: Alignment.center,
        width: 45,
        height: 45,
        decoration: BoxDecoration(
            color: (size == shoeModelProvider.size)
                ? const Color(0xffF1A23A)
                : Colors.white,
            borderRadius: BorderRadius.circular(10),
            boxShadow: [
              if (size == shoeModelProvider.size)
                const BoxShadow(
                  color: Color(0xffF1A23A),
                  blurRadius: 10,
                  offset: Offset(0, 5),
                ),
            ]),
        child: Text(
          size.toString().replaceAll('.0', ''),
          style: TextStyle(
            color: (size == shoeModelProvider.size)
                ? Colors.white
                : const Color(0xffF1A23A),
            fontSize: 16,
            fontWeight: FontWeight.bold,
          ),
        ),
      ),
    );
  }
}
